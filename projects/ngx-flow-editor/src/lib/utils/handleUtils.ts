import {ID, XYCoordinates} from '../types';

export const getHandleAndNodeBoundingBox = (nodeId: ID, handleId: ID): {node: DOMRect, handle: DOMRect} | undefined => {
  const nodeContainer = document.querySelector(`[data-ngx-flow-editor-node-id='${nodeId}']`);
  if (!nodeContainer) {
    return;
  }
  const handleContainer = nodeContainer.querySelector(`[data-ngx-flow-editor-handle-id='${handleId}']`);
  if (!handleContainer) {
    return;
  }
  return {
    node: nodeContainer.getBoundingClientRect(),
    handle: handleContainer.getBoundingClientRect()
  };
};

export const getHandleOffset = (nodeId: ID, handleId: ID, scale: number): XYCoordinates | undefined => {
  const boundingBoxes = getHandleAndNodeBoundingBox(nodeId, handleId);
  if (!boundingBoxes) {
    return;
  }
  const nodeBB = boundingBoxes.node;
  const handleBB = boundingBoxes.handle;
  const offset = {x: (handleBB.x - nodeBB.x), y: (handleBB.y - nodeBB.y)};
  if (handleBB.top <= nodeBB.top) {
    offset.x += handleBB.width / 2;
  } else if (handleBB.bottom >= nodeBB.bottom) {
    offset.x += handleBB.width / 2;
    offset.y += handleBB.height;
  } else if (handleBB.left <= nodeBB.left) {
    offset.y += handleBB.height / 2;
  } else if (handleBB.right >= nodeBB.right) {
    offset.x += handleBB.width;
    offset.y += handleBB.height / 2;
  }
  offset.x /= scale;
  offset.y /= scale;
  return offset;
};
