import { NgModule } from '@angular/core';
import { NgxFlowEditorComponent } from './ngx-flow-editor.component';
import {SvgContainerComponent} from './containers/svg-container.component';
import {NodeRendererComponent} from './containers/node-renderer.component';
import {CommonModule} from '@angular/common';
import {TranslateNodePipe} from './pipes/translate-node.pipe';
import {InputNodeComponent} from './nodes-components/input-node.component';
import {HandleComponent} from './handle/handle.component';
import {OutputNodeComponent} from './nodes-components/output-node.component';
import {InOutNodeComponent} from './nodes-components/in-out-node.component';
import {NodeFactoryDirective} from './containers/node-factory.directive';
import {EdgeRendererComponent} from './containers/edge-renderer.component';
import {SimpleEdgeComponent} from './edges/simple-edge.component';
import {DraggableContainerDirective} from './containers/draggable-container.directive';
import {EdgePathPipe} from './edges/edge-path.pipe';
import {CreateEdgeRendererComponent} from './containers/create-edge-renderer.component';
import {EdgeDuringCreationComponent} from './edges/edge-during-creation.component';



@NgModule({
  declarations: [
    NgxFlowEditorComponent,
    SvgContainerComponent,
    NodeRendererComponent,
    EdgeRendererComponent,
    CreateEdgeRendererComponent,
    TranslateNodePipe,
    InputNodeComponent,
    OutputNodeComponent,
    InOutNodeComponent,
    NodeFactoryDirective,
    DraggableContainerDirective,
    HandleComponent,
    SimpleEdgeComponent,
    EdgeDuringCreationComponent,
    EdgePathPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    NgxFlowEditorComponent
  ]
})
export class NgxFlowEditorModule { }
