import {Component, Input} from '@angular/core';
import {Node, NodeComponent} from '../types';
@Component({
  selector: 'ngx-output-node-component',
  template: `
    <div class="input-node">
        <ngx-handle-component [node]="node" style="position:absolute; top: 0; transform: translate(-50%, -50%); left: 50%" [id]="0"></ngx-handle-component>
    </div>
  `,
  styles: [`
    .input-node {
      background-color: white;
      border: 1px solid black;
      border-radius: 3px;
      width: 150px;
      height: 50px;
      position: relative;
    }
    :host {
      width: 150px;
      height: 50px;
      display: block;
    }
  `]
})

export class OutputNodeComponent implements NodeComponent<OutputNodeData>{
  @Input()
  node!: Node<OutputNodeData>;
  constructor() {
  }
}

type OutputNodeData = {
  labelOutput: string
};
