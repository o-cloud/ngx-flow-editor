import {Component, HostBinding, HostListener, Input} from '@angular/core';
import CreateEdgeController from '../services/create-edge-controller.service';
import {Node, XYCoordinates} from '../types';
import {getHandleAndNodeBoundingBox, getHandleOffset} from '../utils/handleUtils';
import CanvasScaleControllerService from '../services/canvas-scale-controller.service';

@Component({
  selector: 'ngx-handle-component',
  template: ``,
  styles: [`
  :host {
    height: 10px;
    width: 10px;
    background: #555 ;
    border: 1px solid white;
    border-radius: 100%;
    display: block;
  }
  :host:hover {
    background: #36f;
  }
  `]
})

export class HandleComponent {
  @HostBinding('attr.data-ngx-flow-editor-handle-id')
  @Input()
  id!: number;
  @Input()
  node!: Node<any>;

  @HostListener('mousedown', ['$event'])
  onMouseDown(event: MouseEvent): void {
    const offset = this.getOffset();
    if (offset) {
      this.createEdgeController.startDrawing(
        {
          x: this.node.coordinates.x + offset.x,
          y: this.node.coordinates.y + offset.y
        }
        , event, this.node.id, this.id);
      event.stopPropagation();
    } else {
      console.error('cannot compute offset');
    }
  }

  @HostListener('mouseup', ['$event'])
  onMouseUp(event: MouseEvent): void {
    if (this.createEdgeController._isCreatingEdge.value) {
      this.createEdgeController.completeEdge(this.node.id, this.id);
      event.stopPropagation();
    }
  }

  getOffset(): XYCoordinates | undefined {
    const currentScale = this.scaleService.getCurrentScale();
    const offset = getHandleOffset(this.node.id, this.id, currentScale);
    const handleAndNodeBoundingBox = getHandleAndNodeBoundingBox(this.node.id, this.id);
    if (offset && handleAndNodeBoundingBox) {
      if (offset.y < 0) {
        offset.y += (handleAndNodeBoundingBox.handle.height / 2) / currentScale;
      } else if (offset.y > 0) {
        offset.y -= (handleAndNodeBoundingBox.handle.height / 2) / currentScale;
      }
    }
    return offset;
  }

  constructor(private createEdgeController: CreateEdgeController, private scaleService: CanvasScaleControllerService) {
  }
}
