import {ID} from './index';

export type DragNodeEvent = DeltaPosition & {
  nodeId: ID;
};

export type DeltaPosition = {
  deltaX: number;
  deltaY: number;
};
