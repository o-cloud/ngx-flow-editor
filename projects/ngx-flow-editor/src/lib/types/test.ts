import {Component, Type} from '@angular/core';
import {InputNodeComponent} from '../nodes-components/input-node.component';

@Component({template: ``})
class InputNode implements NodeCompoenent<string, any> {
  node!: XNode<string, any>;
}

@Component({template: ``})
class OutputNode implements NodeCompoenent<number, any> {
  node!: XNode<number, any>;
}

export type XNode<T, Map extends NodeMapType> = {
  type: keyof Map
  data?: T,
};

export type NodeCompoenent<T, Map extends NodeMapType> = {
  node: XNode<T, Map>
};

export type NodeMapType = {
  [key: string]: Type<NodeCompoenent<any, any>>
};

function createNodeMap<T extends Record<string, Type<NodeCompoenent<any, any>>>>(arg: T): T {
  return arg;
}

const nodeMap = createNodeMap({
  in: InputNode,
  out: OutputNode,
});
const map2 = createNodeMap({
  input: InputNodeComponent
});

// export type Test<T extends XNode<any, any>> = T['id'];
// export type t2 = Test<{data: 12, id: 'in'}>;
// export type Truc<T extends NodeMapType, X extends keyof T> = T[X] extends Type<NodeCompoenent<infer Q, T>> ? Q : never;

// const t: Truc<typeof nodeMap, 'in'> = {};

// export type T2 = XNode<string, any> & {id: 'str'};
// const t2: T2 = {
//   id: 'test',
//   data: "te"
// };

export type InferNodeComponentType<T extends NodeMapType, Q extends keyof T> = T[Q] extends Type<NodeCompoenent<infer R, T>> ? R : never;
const t3: InferNodeComponentType<typeof nodeMap, 'out'> = {};
const t31: InferNodeComponentType<typeof map2, 'out'> = {};
// InferNodeComponentType<{i: NodeCompoenent<number, T>, j: NodeCompoenent<string, T>}, 'i'> <=> number

export type AllNodeComponentsTypes<T extends NodeMapType> = { [P in keyof T]: InferNodeComponentType<T, P> };
const t4: AllNodeComponentsTypes<typeof nodeMap> = {in: '45', out: '45'};
// AllNodeComponentsTypes<{i: NodeCompoenent<number, T>, j: NodeCompoenent<string, T>}> <=> {i: number, j: string}

export type NodesWithId<T extends NodeMapType> = {[P in keyof AllNodeComponentsTypes<T>]: XNode<AllNodeComponentsTypes<T>[P], T> & {type: P}};
const t5: NodesWithId<typeof nodeMap> = {
  in: {},
  out: {}
};
// NodesWithId<{i: NodeCompoenent<number, T>,j: NodeCompoenent<string, T>}>
// <=> {i: XNode<number, ...> & {id: 'i'}, j: XNode<string, ...> & {id: 'j'}

export type ValuesOf<T> = T[keyof T];

const n: ValuesOf<NodesWithId<typeof nodeMap>> = {
  id: 'in',
  data: 't'
};
// ValuesOf<T5<{i: NodeCompoenent<number, T>, j: NodeCompoenent<string, T>}>>
// <=> (XNode<number, ...> & {id: 'i'}) | (j: XNode<string, ...> & {id: 'j'})

export type Flow<T extends NodeMapType> = {
  nodeMap2: T,
  // node: T[keyof T] extends Type<NodeCompoenent<infer Q, T>> ?  Test<XNode<Q, T>> : never
  // nodes: XNode<any, T>[]
  // node: ValuesOf<T5<typeof nodeMap>>
  nodes: ValuesOf<NodesWithId<typeof nodeMap>>[]
};

// const n0: XNode<number, typeof nodeMap> = {
//   data: 12,
//   id: 'in'
// };

const flow: Flow<typeof nodeMap> = {
  nodeMap2: nodeMap,
  nodes: [
    {data: 12, type: 'out'},
    {data: '12', type: 'in'},
    {data: 12, type: 'in'}
  ]
};
