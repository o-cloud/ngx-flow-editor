import {Type} from '@angular/core';

export type ID = string | number;

export type XYCoordinates = {
  x: number;
  y: number;
};

export type StartEndXYPosition = {
  start: XYCoordinates;
  end: XYCoordinates;
};

export type Node<T> = {
  coordinates: XYCoordinates;
  data?: T;
  type: string;
  id: ID;
};

export type Edge = {
  source: ID;
  target: ID;
  sourceHandle: ID;
  targetHandle: ID;
};

export interface NodeComponent<T> {
  node: Node<T>;
}
export type NodeRenderer = Type<NodeComponent<any>>;

type NodeRenderersMap = {
  [key: string]: NodeRenderer;
};

// looks like it's not required
// export function createNodeMap<T extends Record<string, Type<NodeComponent<any, any>>>>(arg: T): T {
//   return arg;
// }

type InferNodeComponentType<T extends NodeRenderersMap, Q extends keyof T> = T[Q] extends Type<NodeComponent<infer R>> ? R : never;
// InferNodeComponentType<{i: NodeComponent<number, T>, j: NodeComponent<string, T>}, 'i'> <=> number

type AllNodeComponentsTypes<T extends NodeRenderersMap> = { [P in keyof T]: InferNodeComponentType<T, P> };
// AllNodeComponentsTypes<{i: NodeComponent<number, T>, j: NodeComponent<string, T>}> <=> {i: number, j: string}

type NodesWithType<T extends NodeRenderersMap> =
  {[P in keyof AllNodeComponentsTypes<T>]: Node<AllNodeComponentsTypes<T>[P]> & {type: P}};
// NodesWithType<{i: NodeComponent<number, T>,j: NodeComponent<string, T>}>
// <=> {i: Node<number, ...> & {type: 'i'}, j: Node<string, ...> & {type: 'j'}

type ValuesOf<T> = T[keyof T];
export type TypeCheckedNode<T extends NodeRenderersMap> = ValuesOf<NodesWithType<T>>;
// TypeCheckedNode<{i: NodeComponent<number, T>, j: NodeComponent<string, T>}>>
// <=> (Node<number, ...> & {id: 'i'}) | (j: Node<string, ...> & {id: 'j'})

export type Flow<T extends NodeRenderersMap> = {
  nodeMap: T,
  nodes: TypeCheckedNode<T>[],
  edges: Edge[]
};

