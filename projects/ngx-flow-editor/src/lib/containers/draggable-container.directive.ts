import {Directive, ElementRef, EventEmitter, HostBinding, HostListener, Input, OnDestroy, Output} from '@angular/core';
import {DeltaPosition} from '../types/drag-types';
import {XYCoordinates} from '../types';
import CanvasScaleControllerService from '../services/canvas-scale-controller.service';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[draggableContainer]',
})
export class DraggableContainerDirective implements OnDestroy {
  constructor(private el: ElementRef, private scaleService: CanvasScaleControllerService) {
    this.ownerDocument = this.el.nativeElement.ownerDocument;
  }

  @HostBinding('style') hostStyle = `
     -webkit-touch-callout: none;
     -webkit-user-select: none;
     -khtml-user-select: none;
     -moz-user-select: none;
     -ms-user-select: none;
     user-select: none;
  `;

  @Output() move = new EventEmitter<DeltaPosition>();
  lastCoordinates: XYCoordinates = {
    x: 0,
    y: 0
  };

  ownerDocument: Document;

  @Input()
  consumeEvents = true;

  @HostListener('mousedown', ['$event']) onMouseDown(e: MouseEvent): void {
    this.onDragStart(e);
  }

  consumeEvent = (e: MouseEvent): void => {
    if (this.consumeEvents) {
      e.stopPropagation();
    }
  }

  onDragStart = (e: MouseEvent): void => {
    this.consumeEvent(e);
    this.lastCoordinates = {
      x: e.x,
      y: e.y
    };
    this.ownerDocument.addEventListener('mousemove', this.onDrag);
    this.ownerDocument.addEventListener('mouseup', this.onDragStop);
  }

  onDragStop = (e: MouseEvent): void => {
    this.consumeEvent(e);
    this.ownerDocument.removeEventListener('mousemove', this.onDrag);
    this.ownerDocument.removeEventListener('mouseup', this.onDragStop);
  }

  onDrag = (e: MouseEvent): void => {
    this.consumeEvent(e);
    const {x, y} = e;
    const currentScale = this.scaleService.getCurrentScale();
    const delta = {
      deltaX: (x - this.lastCoordinates.x) / currentScale,
      deltaY: (y - this.lastCoordinates.y) / currentScale,
    };
    this.lastCoordinates = {x, y};
    this.move.emit(delta);
  }

  ngOnDestroy(): void {
  }
}
