import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'ngx-svg-container-component',
  template: `
    <svg (mousedown)="mouseDown.emit($event)">
      <ng-content></ng-content>
    </svg>
  `,
  styles: [`
    :host {
      display: block;
    }
    svg {
      height: 100%;
      width: 100%;
    }
  `]
})

export class SvgContainerComponent {
  // tslint:disable-next-line:no-output-native
  @Output()
  mouseDown = new EventEmitter<MouseEvent>();
  constructor() {
  }

}
