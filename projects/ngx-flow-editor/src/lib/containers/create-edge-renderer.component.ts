import {Component, ViewChild} from '@angular/core';
import CreateEdgeController from '../services/create-edge-controller.service';

@Component({
  selector: 'ngx-create-edge-renderer-component',
  template: `
  <ng-template #template>
    <svg:g ngx-edge-during-creation *ngIf="createEdgeController.isCreatingEdge() | async" [position]="(createEdgeController.edgePosition() | async)!">
    </svg:g>
  </ng-template>
  `
})

export class CreateEdgeRendererComponent {
  @ViewChild('template') template: any;
  constructor(public createEdgeController: CreateEdgeController) {
  }
}
