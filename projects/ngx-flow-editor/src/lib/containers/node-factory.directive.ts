import {ComponentFactoryResolver, Directive, Input, OnInit, Type, ViewContainerRef} from '@angular/core';
import {Node, NodeComponent} from '../types';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[nodeFactory]',
})
export class NodeFactoryDirective implements OnInit {
  @Input()
  node!: Node<any>;
  @Input()
  renderer: Type<NodeComponent<any>> | undefined;

  constructor(public viewContainerRef: ViewContainerRef, private componentFactoryResolver: ComponentFactoryResolver) {

  }

  ngOnInit(): void {
    if (this.renderer) {
      const factory = this.componentFactoryResolver.resolveComponentFactory(this.renderer);
      const comp = this.viewContainerRef.createComponent(factory);
      comp.instance.node = this.node;
      this.viewContainerRef.element.nativeElement.previousSibling.dataset.ngxFlowEditorNodeId = this.node.id;
      // this.viewContainerRef.element.nativeElement.previousSibling.addEventListener('resize', (e: any) => console.log(e));
      // console.log(this.viewContainerRef.element.nativeElement.previousSibling.getBoundingClientRect());
    } else {
      console.warn(`node type ${this.node.type} is not present in the nodeTypes object`);
    }
  }

}
