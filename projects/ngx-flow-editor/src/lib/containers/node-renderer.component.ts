import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {Flow, ID} from '../types';
import {DeltaPosition, DragNodeEvent} from '../types/drag-types';


@Component({
  selector: 'ngx-node-renderer-component',
  template: `
  <ng-template #template>
    <svg:g [attr.transform]="node | translateNode" *ngFor="let node of flow.nodes">
        <svg:foreignObject width="1" height="1" style="overflow: visible">
          <div draggableContainer (move)="onNodeMove($event, node.id)">
            <ng-template nodeFactory [node]="node" [renderer]="flow.nodeMap[node.type]"></ng-template>
          </div>
        </svg:foreignObject>
    </svg:g>
  </ng-template>
  `
})

export class NodeRendererComponent {
  @Input()
  flow!: Flow<any>;
  @Output() dragNode = new EventEmitter<DragNodeEvent>();
  @ViewChild('template') template: any;
  constructor() {
  }

  onNodeMove = (e: DeltaPosition, nodeId: ID) => {
    this.dragNode.emit({
      ...e,
      nodeId,
    });
  }
}
