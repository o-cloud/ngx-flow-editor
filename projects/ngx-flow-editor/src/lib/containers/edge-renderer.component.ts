import {Component, Input, ViewChild} from '@angular/core';
import {Flow} from '../types';
import {EDGE_ARROW_SIZE} from '../constants';
@Component({
  selector: 'ngx-edge-renderer-component',
  template: `
    <ng-template #template>
      <svg:defs>
        <svg:marker id="ngx-flow-edge-arrow-head"
                    [attr.markerWidth]="12.5" [attr.markerHeight]="12.5" [attr.viewBox]="'-10 -10 20 20'"
                    [attr.orient]="'auto'" [attr.refX]="-EDGE_ARROW_SIZE" [attr.refY]="0">
          <svg:polyline [attr.stroke]="'#334455'"
                        [attr.stroke-linecap]="'round'"
                        [attr.stroke-linejoin]="'round'"
                        [attr.stroke-width]="1" [attr.fill]="'#334455'" [attr.points]="'-5,-4 0,0 -5,4 -5,-4'"></svg:polyline>
        </svg:marker>
        <svg:marker id="ngx-flow-edge-arrow-head-hover"
                    [attr.markerWidth]="12.5" [attr.markerHeight]="12.5" [attr.viewBox]="'-10 -10 20 20'"
                    [attr.orient]="'auto'" [attr.refX]="-EDGE_ARROW_SIZE" [attr.refY]="0">
          <svg:polyline [attr.stroke]="'#36f'"
                        [attr.stroke-linecap]="'round'"
                        [attr.stroke-linejoin]="'round'"
                        [attr.stroke-width]="1" [attr.fill]="'#36f'" [attr.points]="'-5,-4 0,0 -5,4 -5,-4'"></svg:polyline>
        </svg:marker>
      </svg:defs>
      <svg:g ngx-simple-edges [edge]="edge" [flow]="flow" *ngFor="let edge of this.flow.edges"/>
    </ng-template>
  `,
  styles: [`
  `]
})

export class EdgeRendererComponent {
  @Input()
  flow!: Flow<any>;
  @ViewChild('template') template: any;
  EDGE_ARROW_SIZE = EDGE_ARROW_SIZE;
  constructor() {
  }
}
