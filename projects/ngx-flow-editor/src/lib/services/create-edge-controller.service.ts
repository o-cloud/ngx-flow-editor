import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {Edge, ID, StartEndXYPosition, XYCoordinates} from '../types';
import CanvasScaleControllerService from './canvas-scale-controller.service';
import {Injectable} from '@angular/core';

@Injectable()

export default class CreateEdgeController {
  // tslint:disable-next-line:variable-name
  _isCreatingEdge = new BehaviorSubject<boolean>(false);
  // tslint:disable-next-line:variable-name
  _edgePosition = new BehaviorSubject<StartEndXYPosition | undefined>(undefined);

  createEdgeObservable = new Subject<Edge>();

  lastCoordinates: XYCoordinates = {
    x: 0,
    y: 0
  };

  sourceHandle: {
    nodeId: ID,
    handleId: ID
  } | undefined = undefined;

  constructor(private scaleController: CanvasScaleControllerService) {
  }

  isCreatingEdge = (): Observable<boolean> => this._isCreatingEdge.asObservable();
  edgePosition = (): Observable<StartEndXYPosition | undefined> => this._edgePosition.asObservable();
  startDrawing = (startPoint: XYCoordinates, event: MouseEvent, sourceNodeId: ID, sourceHandleId: ID): void => {
    this.sourceHandle = {
      handleId: sourceHandleId,
      nodeId: sourceNodeId
    };
    this.lastCoordinates = {
      x: event.x,
      y: event.y
    };
    this._edgePosition.next({
      start: startPoint,
      end: startPoint
    });
    this._isCreatingEdge.next(true);
    document.addEventListener('mousemove', this.mouseMoveListener);
    document.addEventListener('mouseup', this.mouseUpListener);
  }

  stopDrawing = (): void => {
    this._isCreatingEdge.next(false);
    this._edgePosition.next(undefined);
    document.removeEventListener('mousemove', this.mouseMoveListener);
    document.removeEventListener('mouseup', this.mouseUpListener);
  }

  completeEdge = (targetNodeId: ID, targetHandleId: ID): void => {
    if (!this.sourceHandle) {
      this.stopDrawing();
      return;
    }
    this.createEdgeObservable.next({
      source: this.sourceHandle.nodeId,
      sourceHandle: this.sourceHandle.handleId,
      target: targetNodeId,
      targetHandle: targetHandleId,
    });
    this.stopDrawing();
  }

  private mouseUpListener = (e: MouseEvent): void => {
    this.stopDrawing();
  }

  private mouseMoveListener = (e: MouseEvent): void => {
    const start = this._edgePosition.value?.start;
    const end = this._edgePosition.value?.end;
    const currentScale = this.scaleController.getCurrentScale();
    const delta = {
      x: (e.x - this.lastCoordinates.x) / currentScale,
      y: (e.y - this.lastCoordinates.y) / currentScale
    };
    this.lastCoordinates = {
      x: e.x,
      y: e.y
    };
    if (start && end) {
      this._edgePosition.next({
        start,
        end: {
          x: end.x + delta.x,
          y: end.y + delta.y
        }
      });
    }
  }
}
