import {BehaviorSubject, Observable} from 'rxjs';
import {clamp} from '../utils/mathUtils';
import {Injectable} from '@angular/core';
@Injectable()

export default class CanvasScaleControllerService {
  private scale = 1;
  private minScale = 0.3;
  private maxScale = 3;
  private scaleSubject = new BehaviorSubject<number>(this.scale);

  getScale = (): Observable<number> => this.scaleSubject.asObservable();

  getCurrentScale = () => this.scale;

  onWheel = (e: WheelEvent) => {
    let newScale = this.scale;
    if (e.deltaY > 0) {
      newScale -= 0.1;
    } else {
      newScale += 0.1;
    }
    this.scale = clamp(newScale, this.minScale, this.maxScale);
    this.scaleSubject.next(this.scale);
  }
}
