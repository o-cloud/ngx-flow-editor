import {XYCoordinates} from '../types';
import {BehaviorSubject, Observable} from 'rxjs';
import {Injectable} from '@angular/core';

@Injectable()
export default class CanvasOffsetControllerService {
  private offset: XYCoordinates = {x: 0, y: 0};
  private offsetSubject = new BehaviorSubject<XYCoordinates>(this.offset);

  private lastMouseCoordinates: XYCoordinates = {
    x: 0,
    y: 0
  };

  getOffset = (): Observable<XYCoordinates> => this.offsetSubject.asObservable();

  startDrag = (e: MouseEvent): void => {
    this.lastMouseCoordinates = {
      x: e.x,
      y: e.y
    };
    document.addEventListener('mousemove', this.onMouseMove);
    document.addEventListener('mouseup', this.onMouseUp);
  }

  stopDrag = (): void => {
    document.removeEventListener('mousemove', this.onMouseMove);
    document.removeEventListener('mouseup', this.onMouseUp);
  }

  private onMouseUp = (): void => {
    this.stopDrag();
  }

  private onMouseMove = (e: MouseEvent): void => {
    const delta = {
      x: e.x - this.lastMouseCoordinates.x,
      y: e.y - this.lastMouseCoordinates.y
    };
    this.lastMouseCoordinates = {
      x: e.x,
      y: e.y
    };
    const currentOffset = this.offset;
    this.offset = {
      x: currentOffset.x + delta.x,
      y: currentOffset.y + delta.y
    };
    this.offsetSubject.next(this.offset);
  }
}
