import {Pipe, PipeTransform} from '@angular/core';
import {Edge, Flow, XYCoordinates} from '../types';
import {getHandleOffset} from '../utils/handleUtils';
import {EDGE_ARROW_SIZE} from '../constants';

@Pipe({name: 'edgePath', pure: false})
export class EdgePathPipe implements PipeTransform {
  minimalControlPointsDistance = 30;

  transform(edge: Edge, flow: Flow<any>, scale: number | null): string {
    if (!scale) {
      scale = 1;
    }
    const sourceNode = flow.nodes.find(n => n.id === edge.source);
    const targetNode = flow.nodes.find(n => n.id === edge.target);
    if (!sourceNode || !targetNode) {
      return '';
    }

    const offsets = this.getHandlesOffsets(edge, scale);
    if (!offsets) {
      return '';
    }
    const sourceHandleOffset = offsets.source;
    const targetHandleOffset = offsets.target;

    const sourcePoint = {x: sourceNode.coordinates.x + sourceHandleOffset.x, y: sourceNode.coordinates.y + sourceHandleOffset.y};
    const targetPoint = {
      x: targetNode.coordinates.x + targetHandleOffset.x,
      y: targetNode.coordinates.y + targetHandleOffset.y - EDGE_ARROW_SIZE
    };
    let distY = Math.abs((targetPoint.y - sourcePoint.y) / 2);
    distY = Math.max(distY, this.minimalControlPointsDistance);

    const controlPoint1 = {
      x: sourcePoint.x,
      y: sourcePoint.y + distY,
    };

    const controlPoint2 = {
      x: targetPoint.x,
      y: targetPoint.y - distY,
    };
    return `M ${sourcePoint.x} ${sourcePoint.y} C ${controlPoint1.x} ${controlPoint1.y} ${controlPoint2.x} ${controlPoint2.y} ${targetPoint.x} ${targetPoint.y}`;

  }

  getHandlesOffsets = (edge: Edge, scale: number): { source: XYCoordinates, target: XYCoordinates } | undefined => {
    const source = getHandleOffset(edge.source, edge.sourceHandle, scale);
    const target = getHandleOffset(edge.target, edge.targetHandle, scale);
    if (source && target) {
      return {source, target};
    } else {
      return undefined;
    }
  }
}
