import {Component, Input, OnInit} from '@angular/core';
import {Edge, Flow} from '../types';
import CanvasScaleControllerService from '../services/canvas-scale-controller.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'g[ngx-simple-edges]',
  template: `
    <svg:path [attr.d]="edge | edgePath: flow : (scaleService.getScale() | async)" />
  `,
  styles: [
    `
      :host {
        stroke: #334455;
        stroke-width: 2;
        fill: transparent;
      }
      :host > path {
        marker-end: url(#ngx-flow-edge-arrow-head)
      }

      :host:hover {
        stroke: #36f;
        cursor: default;
      }
      :host:hover > path {
        marker-end: url(#ngx-flow-edge-arrow-head-hover)
      }

      :host.selected {
        stroke: #36f;
      }
    `
  ]
})

export class SimpleEdgeComponent implements OnInit {
  @Input()
  edge!: Edge;
  @Input()
  flow!: Flow<any>;

  constructor(public scaleService: CanvasScaleControllerService) {
  }

  ngOnInit(): void {

  }
}
