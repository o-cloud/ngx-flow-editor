import {Component, Input} from '@angular/core';
import {StartEndXYPosition} from '../types';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'g[ngx-edge-during-creation]',
  template: `
    <svg:defs>
      <svg:marker id="ghost-arrow-head" markerWidth="10" markerHeight="8" refX="6" refY="4" orient="auto" markersUnits="userSpaceOnUse">
        <svg:path d="M 0 0 L 6 4 L 0 8" stroke-width="1" style="fill: transparent;stroke: #345;stroke-width: 1;"></svg:path>
      </svg:marker>
    </svg:defs>
    <svg:line
      [attr.x1]="position.start.x"
      [attr.x2]="position.end.x"
      [attr.y1]="position.start.y"
      [attr.y2]="position.end.y"
      class="line"
    >
    </svg:line>
  `,
  styles: [
    `
      :host {
        stroke: #345;
        stroke-width: 2;
        fill: transparent
      }
      line {
        marker-end: url(#ghost-arrow-head);
        stroke-dasharray: 5;
        animation: dash .5s linear infinite;
        -webkit-animation: dash .5s linear infinite;
      }
      @keyframes dash {
        to {
          stroke-dashoffset: 20;
        }
      }
    `
  ]
})

export class EdgeDuringCreationComponent {
  @Input()
  position!: StartEndXYPosition;

}
