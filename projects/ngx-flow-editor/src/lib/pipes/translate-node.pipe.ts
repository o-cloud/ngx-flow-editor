import { Pipe, PipeTransform } from '@angular/core';
import {Node} from '../types';

@Pipe({name: 'translateNode', pure: false})
export class TranslateNodePipe implements PipeTransform {
  transform(node: Node<any>): string {
    return `translate(${node.coordinates.x}, ${node.coordinates.y})`;
  }
}
