import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Edge, Flow, XYCoordinates} from './types';
import {DragNodeEvent} from './types/drag-types';
import CreateEdgeController from './services/create-edge-controller.service';
import {Subscription} from 'rxjs';
import CanvasOffsetControllerService from './services/canvas-offset-controller.service';
import CanvasScaleControllerService from './services/canvas-scale-controller.service';

@Component({
  selector: 'ngx-flow-editor',
  template: `
    <ngx-node-renderer-component #nodeRenderer [flow]="flow" (dragNode)="onDragNode($event)"></ngx-node-renderer-component>
    <ngx-edge-renderer-component #edgeRenderer [flow]="flow"></ngx-edge-renderer-component>
    <ngx-create-edge-renderer-component #createEdgeRenderer></ngx-create-edge-renderer-component>
    <ngx-svg-container-component style="width: 100%; height: 100%" (mouseDown)="canvasOffsetController.startDrag($event)" (wheel)="canvasScaleController.onWheel($event)">
      <svg:g [attr.transform]="'translate(' + canvasOffset.x + ',' + canvasOffset.y + ') scale(' + canvasScale + ')'">
        <ng-container [ngTemplateOutlet]="nodeRenderer.template"></ng-container>
        <ng-container [ngTemplateOutlet]="edgeRenderer.template"></ng-container>
        <ng-container [ngTemplateOutlet]="createEdgeRenderer.template"></ng-container>
      </svg:g>
    </ngx-svg-container-component>
  `,
  styles: [`
    :host {
      display: block;
    }
  `
  ],
  providers: [CreateEdgeController, CanvasOffsetControllerService, CanvasScaleControllerService]
})
export class NgxFlowEditorComponent implements OnInit, OnDestroy {
  @Input()
  flow!: Flow<any>;
  @Output()
  nodeDrag = new EventEmitter<DragNodeEvent>();
  @Output()
  createEdge = new EventEmitter<Edge>();

  subscriptions = new Subscription();

  canvasOffset: XYCoordinates = {x: 0, y: 0};
  canvasScale = 1;

  onDragNode = (e: DragNodeEvent) => {
    this.nodeDrag.emit(e);
  }

  constructor(
    private createEdgeController: CreateEdgeController,
    public canvasOffsetController: CanvasOffsetControllerService,
    public canvasScaleController: CanvasScaleControllerService) {
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.createEdgeController.createEdgeObservable.subscribe(e => this.createEdge.emit(e))
    );
    this.subscriptions.add(
      this.canvasOffsetController.getOffset().subscribe(o => this.canvasOffset = o)
    );
    this.subscriptions.add(
      this.canvasScaleController.getScale().subscribe(s => this.canvasScale = s)
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
