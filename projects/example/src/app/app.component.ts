import {Component} from '@angular/core';
import {Edge, Flow, TypeCheckedNode} from '../../../ngx-flow-editor/src/lib/types';
import {InputNodeComponent} from '../../../ngx-flow-editor/src/lib/nodes-components/input-node.component';
import {OutputNodeComponent} from '../../../ngx-flow-editor/src/lib/nodes-components/output-node.component';
import {InOutNodeComponent} from '../../../ngx-flow-editor/src/lib/nodes-components/in-out-node.component';
import {DragNodeEvent} from '../../../ngx-flow-editor/src/lib/types/drag-types';

const nodeRenderers = {
  input: InputNodeComponent,
  output: OutputNodeComponent,
  inOut: InOutNodeComponent,
};

type AppNode = TypeCheckedNode<typeof nodeRenderers>;

@Component({
  selector: 'app-root',
  template: `
    <ngx-flow-editor [flow]="flow" style="height: 90vh; width: 100%"
                     (nodeDrag)="onDragNode($event)"
                     (createEdge)="onCreateEdge($event)">
    </ngx-flow-editor>
  `
})
export class AppComponent {

  n: AppNode = {
    type: 'input',
    coordinates: {x: 200, y: 0},
    id: 7
  };
  flow: Flow<typeof nodeRenderers> = {
    nodeMap: nodeRenderers,
    edges: [
      {source: 1, target: 2, sourceHandle: 0, targetHandle: 0}
    ],
    nodes: [
      {coordinates: {x: 0, y: 0}, type: 'input', data: {labelInput: 'test'}, id: 1},
      {coordinates: {x: 450, y: 200}, type: 'output', id: 2},
      {coordinates: {x: 25, y: 400}, type: 'input', id: 3},
      {coordinates: {x: 150, y: 500}, type: 'inOut', id: 4},
      {
        type: 'input',
        coordinates: {x: 0, y: 150},
        id: 5
      },
      {
        type: 'output',
        coordinates: {x: 400, y: 0},
        id: 6
      },
      this.n
    ]
  };

  onDragNode = (e: DragNodeEvent): void => {
    const index = this.flow.nodes.findIndex(n => n.id === e.nodeId);
    if (index >= 0) {
      this.flow.nodes[index].coordinates.x += e.deltaX;
      this.flow.nodes[index].coordinates.y += e.deltaY;
    }
  }

  onCreateEdge = (e: Edge): void => {
    console.log('createEdge', e);
    if (e.target !== e.source) {
      this.flow.edges.push(e);
    }
  }
}
